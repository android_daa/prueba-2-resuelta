package com.example.duoc.resultadoprueba;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.duoc.resultadoprueba.fragments.AgregarPersonaFragment;
import com.example.duoc.resultadoprueba.fragments.EliminarPersonaFragment;
import com.example.duoc.resultadoprueba.fragments.ListarPersonaFragment;


public class MenuPrincipalActivity extends ActionBarActivity {

    AgregarPersonaFragment agregarPersonaFragment;
    EliminarPersonaFragment eliminarPersonaFragment;
    ListarPersonaFragment listarPersonaFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView txtSalir = (TextView)toolbar.findViewById(R.id.txtSalir);
        txtSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    public void onClickAgregar(View v){
        if(agregarPersonaFragment == null){
            agregarPersonaFragment = new AgregarPersonaFragment();
        }

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.content, agregarPersonaFragment, AgregarPersonaFragment.class.getSimpleName());
        ft.commit();
    }

    public void onClickEliminar(View v){
        if(eliminarPersonaFragment == null){
            eliminarPersonaFragment = new EliminarPersonaFragment();
        }
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.content, eliminarPersonaFragment, EliminarPersonaFragment.class.getSimpleName());
        ft.commit();
    }

    public void onClickListar(View v){
        if(listarPersonaFragment == null){
            listarPersonaFragment = new ListarPersonaFragment();
        }
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.content, listarPersonaFragment, ListarPersonaFragment.class.getSimpleName());
        ft.commit();
    }
}
