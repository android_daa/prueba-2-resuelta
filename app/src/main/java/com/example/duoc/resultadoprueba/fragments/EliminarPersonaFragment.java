package com.example.duoc.resultadoprueba.fragments;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.duoc.resultadoprueba.BD;
import com.example.duoc.resultadoprueba.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class EliminarPersonaFragment extends Fragment {


private EditText txtNombre;
    private Button btnEliminar;

    public EliminarPersonaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_eliminar_persona, container, false);

        txtNombre = (EditText)v.findViewById(R.id.txtNombre);
        btnEliminar = (Button)v.findViewById(R.id.btnEliminar);

        btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(BD.eliminarPersona(txtNombre.getText().toString())){
                    Toast.makeText(getActivity(), "Registr Eliminado", Toast.LENGTH_LONG).show();
                    txtNombre.setText("");
                }else{
                    Toast.makeText(getActivity(), "No existe el nombre", Toast.LENGTH_LONG).show();
                }
            }
        });

        return v;
    }


}
