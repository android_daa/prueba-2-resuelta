package com.example.duoc.resultadoprueba.fragments;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.duoc.resultadoprueba.BD;
import com.example.duoc.resultadoprueba.R;
import com.example.duoc.resultadoprueba.entidades.Persona;

/**
 * A simple {@link Fragment} subclass.
 */
public class AgregarPersonaFragment extends Fragment {


    public AgregarPersonaFragment() {
        // Required empty public constructor
    }

    private EditText txtNombre;
    private EditText txtEmail;
    private EditText txtEdad;
    private EditText txtTelefono;

    private Button btnAgregar;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_agregar_persona, container, false);

        txtNombre = (EditText) v.findViewById(R.id.txtNombre);
        txtEmail = (EditText) v.findViewById(R.id.txtEmail);
        txtEdad = (EditText) v.findViewById(R.id.txtEdad);
        txtTelefono = (EditText) v.findViewById(R.id.txtTelefono);
        btnAgregar = (Button) v.findViewById(R.id.btnAgregar);

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (txtNombre.getText().toString().length() > 1 &&
                        txtEmail.getText().toString().length() > 1 &&
                        txtEdad.getText().toString().length() > 1 &&
                        txtTelefono.getText().toString().length() > 1) {

                    Persona p = new Persona();
                    p.setNombre(txtNombre.getText().toString());
                    p.setEmail(txtEmail.getText().toString());
                    p.setEdad(txtEdad.getText().toString());
                    p.setTelefono(txtTelefono.getText().toString());
                    BD.agregarPersona(p);

                    txtNombre.setText("");
                    txtEmail.setText("");
                    txtEdad.setText("");
                    txtTelefono.setText("");


                } else {
                    Toast.makeText(getActivity(), "Ingrese todos los valores", Toast.LENGTH_LONG).show();
                }


            }
        });

        return v;
    }


}
