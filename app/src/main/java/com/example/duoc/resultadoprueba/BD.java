package com.example.duoc.resultadoprueba;

import com.example.duoc.resultadoprueba.entidades.Persona;

import java.util.ArrayList;

/**
 * Created by Duoc on 27-11-2015.
 */
public class BD {
    private static ArrayList<Persona> values = new ArrayList<>();

    public static void agregarPersona(Persona p) {
        values.add(p);
    }

    public static boolean eliminarPersona(String nombre) {
        boolean isDelete = false;
        for (int x = 0; x < values.size(); x++) {
            if (values.get(x).getNombre().equals(nombre)) {
                values.remove(x);
                isDelete = true;
                break;
            }
        }
        return isDelete;
    }

    public static ArrayList<Persona> getValues(){
            return values;
    }
}
