package com.example.duoc.resultadoprueba.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.duoc.resultadoprueba.R;
import com.example.duoc.resultadoprueba.entidades.Persona;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Duoc on 27-11-2015.
 */
public class PersonaAdapter extends BaseAdapter {
    private List<Persona> dataSet;
    private Context context;

    public PersonaAdapter(Context context, ArrayList<Persona> dataSet) {
        this.context = context;
        this.dataSet = dataSet;
    }

    @Override
    public int getCount() {
        return dataSet.size();
    }

    @Override
    public Object getItem(int position) {
        return dataSet.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolderItem viewHolder;


        if (convertView == null) {
            // Create a new view into the list.
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item, parent, false);

            viewHolder = new ViewHolderItem();
            viewHolder.txtNombre = (TextView) convertView.findViewById(R.id.txtNombre);
            viewHolder.txtEmail= (TextView) convertView.findViewById(R.id.txtEmail);
            viewHolder.txtEdad= (TextView) convertView.findViewById(R.id.txtEdad);
            viewHolder.txtTelefono= (TextView) convertView.findViewById(R.id.txtTelefono);

            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolderItem)convertView.getTag();
        }

        Persona persona = (Persona)getItem(position);
        if(persona != null){
            viewHolder.txtNombre.setText(persona.getNombre());
            viewHolder.txtEmail.setText(persona.getEmail());
            viewHolder.txtEdad.setText(persona.getEdad());
            viewHolder.txtTelefono.setText(persona.getTelefono());
        }

        return convertView;
    }

    static class ViewHolderItem {
        TextView txtNombre;
        TextView txtEmail;
        TextView txtEdad;
        TextView txtTelefono;
    }

}