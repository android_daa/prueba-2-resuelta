package com.example.duoc.resultadoprueba.fragments;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.duoc.resultadoprueba.BD;
import com.example.duoc.resultadoprueba.R;
import com.example.duoc.resultadoprueba.adapter.PersonaAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListarPersonaFragment extends Fragment {


    ListView lsPersonas;


    public ListarPersonaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_listar_persona, container, false);

        lsPersonas = (ListView)v.findViewById(R.id.lsPersonas);

        lsPersonas.setAdapter(new PersonaAdapter(getActivity(), BD.getValues()));

        return v;
    }


}
